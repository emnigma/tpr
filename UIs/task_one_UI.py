# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UIs/task_one_UI.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(311, 211)
        Dialog.setMinimumSize(QtCore.QSize(311, 211))
        Dialog.setMaximumSize(QtCore.QSize(311, 211))
        Dialog.setAutoFillBackground(False)
        self.start_button = QtWidgets.QPushButton(Dialog)
        self.start_button.setGeometry(QtCore.QRect(100, 170, 111, 41))
        self.start_button.setObjectName("start_button")
        self.filepath_input = QtWidgets.QLineEdit(Dialog)
        self.filepath_input.setGeometry(QtCore.QRect(40, 60, 231, 21))
        self.filepath_input.setObjectName("filepath_input")
        self.info1 = QtWidgets.QTextBrowser(Dialog)
        self.info1.setEnabled(True)
        self.info1.setGeometry(QtCore.QRect(40, 10, 231, 41))
        self.info1.setInputMethodHints(QtCore.Qt.ImhNone)
        self.info1.setObjectName("info1")
        self.matrix_count_input = QtWidgets.QLineEdit(Dialog)
        self.matrix_count_input.setGeometry(QtCore.QRect(40, 140, 231, 21))
        self.matrix_count_input.setObjectName("matrix_count_input")
        self.info2 = QtWidgets.QTextBrowser(Dialog)
        self.info2.setEnabled(True)
        self.info2.setGeometry(QtCore.QRect(40, 90, 231, 41))
        self.info2.setInputMethodHints(QtCore.Qt.ImhNone)
        self.info2.setObjectName("info2")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Первый тип"))
        self.start_button.setText(_translate("Dialog", "Начать"))
        self.info1.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Пожалуйста, введите путь к папке для сгенерированного файли</p></body></html>"))
        self.info2.setHtml(_translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Введите количество матриц для генерации</p></body></html>"))
