import numpy as np

from fractions import Fraction

from random import randint, seed
import itertools

# seed('11343')


def convertToFractions(matrix):
    'Converts given matrix with string values to matrix with fractions'
    fractioned_matrix = matrix.copy()
    for i, row in enumerate(matrix):
        for j, item in enumerate(row):
            fractioned_matrix[i][j] = Fraction(item)
    return matrix


def genNumberForMatrix():
    '''
    Return a_ij or 1 / a_ij (50/50)

    Component a_ij is chosen from even distribution(1, ... , 9)
    '''
    if randint(0, 1):
        return randint(1, 9)
    else:
        return Fraction(1, randint(1, 9))


def generateMatrix(rank=None):
    'Returns matrix of pair copmarisons'
    if rank is None:
        rank = 7
    matrix = [[0 for i in range(rank)] for j in range(rank)]
    for i in range(rank):
        for j in range(rank):
            if i < j:
                matrix[i][j] = genNumberForMatrix()
            elif i > j:
                num = matrix[j][i].numerator
                den = matrix[j][i].denominator
                matrix[i][j] = Fraction(den, num)
            elif i == j:
                matrix[i][j] = 1
    return matrix


def generateInOrderMatrix(left_border, right_border, rank=None):
    if rank is None:
        rank = 3
    numbers = []
    for iteration in range(11):
        numbers.append(left_border + (right_border-left_border)/10 * iteration)

    for i in itertools.product(numbers, repeat=rank**2):
        yield i


class mpc:
    'This class implements matrix of pair comparisons'

    MATRIX_RANK = 7

    def __init__(self, src=None):
        if src is None:
            self._matrix = generateMatrix(self.MATRIX_RANK)
        else:
            self._matrix = convertToFractions(src)

        self._rank = len(self._matrix[0])

        self._row_mults = []

        for row in self._matrix:
            local_row_mult = Fraction(1, 1)
            for item in row:
                local_row_mult *= item
            self._row_mults.append(local_row_mult)

        self._row_mults = np.array(self._row_mults)

        for i, item in enumerate(self._row_mults):
            self._row_mults[i] = pow(item, 1 / self._rank)

        norm = sum(self._row_mults)
        for i, item in enumerate(self._row_mults):
            self._row_mults[i] /= norm

    def display(self):
        for row in self._matrix:
            for item in row:
                if item.denominator == 1:
                    print(item.numerator, ' ', end='')
                else:
                    print(f'{item.numerator}/{item.denominator}', ' ',  end='')
            print()

    @property
    def matrix(self):
        return self._matrix

    @property
    def rank(self):
        return self._rank

    @property
    def eigen_vector(self):
        return self._row_mults


def genEigens(n):
    'Returns vector of calculated eigen vectors from n random matrices'
    eigens = []
    for iteration in range(n):

        m = mpc()
        eigens.append(m.eigen_vector)

    return np.array(eigens)


def getMeanVector(matrix):
    'Returns vector of math expectations on 0 axis'
    means = []
    for row in matrix:
        means.append(
            np.mean(row)
        )
    return means


def getVarianceVector(matrix):
    'Returns vector of dispersions on 0 axis'
    variances = []
    for row in matrix:
        variances.append(
            np.var(row)
        )
    return variances


def getStd(matrix):
    'Returns vector of standart deviations on 0 axis'
    stds = []
    for row in matrix:
        stds.append(
            np.std(row)
        )
    return stds


if __name__ == "__main__":
    seed('11343')

    # print(set(getMeanVector(genEigens(50000))))

    for i in generateInOrderMatrix(1, 11):
        print(i)
