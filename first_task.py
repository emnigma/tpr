import pandas as pd
import sys
import os

from PyQt5.QtWidgets import QApplication, QDialog
import UIs.task_one_UI

import statistics as stat


class App(QDialog, UIs.task_one_UI.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.start_button.clicked.connect(self.create_stats)

    def create_stats(self):

        try:
            matrix_count = int(self.matrix_count_input.text())
        except ValueError:
            return

        path_to_save = self.filepath_input.text()
        if not os.path.exists(path_to_save):
            print('wrong path')
            return

        eigs = stat.genEigens(matrix_count)

        df = pd.DataFrame(
                eigs,
                columns=[i + 1 for i in range(len(eigs[0]))],
            )

        df.index = pd.MultiIndex.from_arrays(
                [['eigen vectors'] * len(df.index), df.index],
                names=(None, None)
            )

        # сформировали массив собственных значений

        df['expectation'] = stat.getMeanVector(eigs)
        df['dispersion'] = stat.getVarianceVector(eigs)
        df['standart deviation'] = stat.getStd(eigs)

        if os.path.exists(path_to_save):
            df.to_csv(path_to_save + '/task1_report.csv')
        else:
            print('wrong path')


def launch_app():
    app = QApplication(sys.argv)
    window = App()
    window.show()
    app.exec_()


# if __name__ == "__main__":
#     launch_app()
