import statistics as stat
import numpy as np

test1 = [
    ['1',   '1/6', '2'],
    ['4',   '1',   '7'],
    ['1/4', '1/9', '1']
]

test2 = [
    ['1',   '2',   '4',   '7'],
    ['1/2', '1',   '3',   '9'],
    ['1/4', '1/3', '1',   '7'],
    ['1/7', '1/9', '1/7', '1']
]

if __name__ == "__main__":
    print()
    m1 = stat.mpc(test1)
    print('данные с тестового образца 1:')
    print(f'собственный вектор: {m1.eigen_vector}')
    print(f'сумма элементов(вектор нормирован): {sum(m1.eigen_vector)}')
    print(f'матожидание: {np.mean(m1.eigen_vector)}')
    print(f'дисперсия: {np.var(m1.eigen_vector, axis=0)}')
    print(f'среднеквадратичное отклонение: {np.std(m1.eigen_vector)}')

    print()
    m2 = stat.mpc(test2)
    print('данные с тестового образца 2:')
    print(f'собственный вектор: {m2.eigen_vector}')
    print(f'сумма элементов(вектор нормирован): {sum(m2.eigen_vector)}')
    print(f'матожидание: {np.mean(m2.eigen_vector)}')
    print(f'дисперсия: {np.var(m2.eigen_vector)}')
    print(f'среднеквадратичное отклонение: {np.std(m2.eigen_vector)}')
